package main.encryption;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EncryptionTest {
    @Test
    public void before() {
        String string = "blabla";

        String encrypted = Encryption.encrypt(string);
        System.out.println(encrypted);

        String decrypted = Encryption.decrypt(encrypted);
        System.out.println(decrypted);

        assertEquals(string, decrypted);
    }
}