package main.encryption;

import main.config.Configuration;

public class Encryption {
    public static String encrypt(String message) {
        int hashIndex = makeHash();

        StringBuilder out = new StringBuilder();

        for (int i = 0; i < message.length(); i++) {
            char c = message.charAt(i);
            c += hashIndex;
            out.append(c);
        }

        return out.toString();
    }

    private static int makeHash() {
        String password = Configuration.getPassword();
        int out = 0;
        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);
            out += c;
        }
        return out;
    }

    public static String decrypt(String message) {
        int hashIndex = makeHash();

        StringBuilder out = new StringBuilder();

        for (int i = 0; i < message.length(); i++) {
            char c = message.charAt(i);
            c -= hashIndex;
            out.append(c);
        }

        return out.toString();
    }
}
