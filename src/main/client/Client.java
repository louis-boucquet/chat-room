package main.client;

import main.config.Configuration;
import main.protocol.Message;
import main.protocol.Sendable;
import main.protocol.Start;
import main.protocol.Stop;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        new Client().run();
    }

    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Scanner scanner;

    private String userName;

    private void run() throws IOException {
        Socket socket = new Socket(Configuration.getAddress(), Configuration.getPort());

        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());

        scanner = new Scanner(System.in);

        pickUserName();
        StartMessaging();
    }

    private void pickUserName() throws IOException {
        System.out.print("give a username: ");

        userName = scanner.nextLine();
        out.writeObject(new Start(userName));

        Start start = nextStart();

        while (!start.isGood()) {
            System.out.println("name is already in use");
            System.out.print("give a username: ");

            userName = scanner.nextLine();
            out.writeObject(new Start(userName));

            start = nextStart();
        }
    }

    private Start nextStart() {
        Start start = null;
        while (true) {
            try {
                start = (Start) in.readObject();
                return start;
            } catch (Exception ignored) {
            }
        }
    }

    private void StartMessaging() throws IOException {
        System.out.println("you can start messaging now");
        Listener listener = new Listener(in);
        listener.start();

        while (true) {
            String line = scanner.nextLine();
            Sendable toSend;
            if (line.equals("quit")) {
                toSend = new Stop();
                System.exit(0);
            } else {
                toSend = new Message(line, userName);
            }

            out.writeObject(toSend);
        }
    }
}
