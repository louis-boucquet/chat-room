package main.client;

import java.io.IOException;
import java.io.ObjectInputStream;

public class Listener extends Thread implements Runnable {

    private final ObjectInputStream in;
    private boolean running;

    public Listener(ObjectInputStream in) {
        this.in = in;
        running = true;
    }

    @Override
    public void run() {
        while (running) {
            try {
                System.out.println(in.readObject());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void destruct() {
        running = false;
        System.out.println("destructing listener thread");
    }
}
