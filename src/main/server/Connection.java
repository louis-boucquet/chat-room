package main.server;

import main.protocol.Message;
import main.protocol.Sendable;
import main.protocol.Start;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.function.Consumer;
import java.util.function.Function;

public class Connection extends Thread implements Runnable {
    private Function<Connection, Boolean> availabilityChecker;
    private Consumer<Message> messageAccepter;
    private Consumer<Connection> destructor;

    private Socket socket;

    private ObjectOutputStream out;
    private ObjectInputStream in;

    private String userName;

    public Connection(
            Socket socket,
            Function<Connection, Boolean> availabilityChecker,
            Consumer<Message> messageAccepter,
            Consumer<Connection> destructor
    ) throws IOException {
        this.availabilityChecker = availabilityChecker;
        this.messageAccepter = messageAccepter;
        this.destructor = destructor;

        this.socket = socket;

        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());
    }

    public void run() {
        try {
            while (true) {
                try {
                    Sendable income = (Sendable) in.readObject();
                    income.handle(this);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            destruct();
        }
    }

    // Dispatch

    public void acceptMessage(Message message) {
        messageAccepter.accept(message);
    }

    public void acceptStart(Start start) {
        userName = start.getName();
        boolean isOk = availabilityChecker.apply(this);
        start.setGood(isOk);
        try {
            out.writeObject(start);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void destruct() {
        try {
            socket.close();
            destructor.accept(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Own

    public void sendMessage(Message message) {
        try {
            out.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getUserName() {
        return userName;
    }
}
