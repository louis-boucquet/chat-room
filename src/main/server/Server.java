package main.server;

import main.config.Configuration;
import main.protocol.Message;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Server {
    public static void main(String[] args) throws IOException {
        new Server().run();
    }

    private Set<Connection> connections;

    private Server() {
        connections = Collections.synchronizedSet(new HashSet<>());
    }

    private void run() throws IOException {
        ServerSocket serverSocket = new ServerSocket(Configuration.getPort());

        while (true) {
            Socket socket = serverSocket.accept();

            System.out.println("new connection");

            Connection connection = new Connection(
                    socket,
                    this::checkAvailability,
                    this::receiveMessage,
                    this::deleteConnection);
            connection.start();

            connections.add(connection);
        }
    }

    private synchronized Boolean checkAvailability(Connection toCheck) {
        for (Connection connection : connections) {
            if (connection != toCheck && connection.getUserName().equals(toCheck.getUserName())) {
                return false;
            }
        }
        return true;
    }

    private synchronized void receiveMessage(Message message) {
        connections.forEach(connection -> connection.sendMessage(message));
    }

    private synchronized void deleteConnection(Connection connection) {
        connections.remove(connection);
        System.out.println("new connection");
    }
}
