package main.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Configuration {
    private static Config config = new Config();

    public static int getPort() {
        return config.port;
    }

    public static String getAddress() {
        return config.address;
    }

    public static String getPassword() {
        return config.password;
    }

    static class Config {
        String password;
        String address;
        int port;

        Config() {
            try {
                Scanner scanner = new Scanner(new File("config.config"));
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    String[] options = line.split(":");
                    set(options[0], options[1]);
                }

            } catch (FileNotFoundException e) {
                System.err.println("no config.config file found");
                System.exit(1);
            }
        }

        private void set(String variable, String value) {
            if (variable.equals("address")) setAddress(value);
            if (variable.equals("port")) setPort(value);
            if (variable.equals("password")) setPassword(value);
        }

        private void setAddress(String value) {
            address = value;
        }

        private void setPort(String value) {
            port = Integer.parseInt(value);
        }

        private void setPassword(String value) {
            password = value;
        }
    }
}
