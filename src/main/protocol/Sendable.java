package main.protocol;

import main.server.Connection;
import main.server.Server;

public abstract class Sendable {
    public abstract void handle(Connection connection);
}
