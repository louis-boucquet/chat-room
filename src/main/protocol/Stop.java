package main.protocol;

import main.server.Connection;

import java.io.Serializable;

public class Stop extends Sendable implements Serializable {
    private static final long serialVersionUID = 2L;
    @Override
    public void handle(Connection connection) {
        connection.destruct();
    }
}
