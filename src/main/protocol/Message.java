package main.protocol;

import main.encryption.Encryption;
import main.server.Connection;
import main.server.Server;

import java.io.Serializable;

public class Message extends Sendable implements Serializable {
    private static final long serialVersionUID = 1L;

    private final String message;
    private final String name;

    public Message(String message, String name) {
        this.message = Encryption.encrypt(message);
        this.name = Encryption.encrypt(name);
    }

    @Override
    public void handle(Connection connection) {
        connection.acceptMessage(this);
    }

    @Override
    public String toString() {
        String name = Encryption.decrypt(this.name);
        String message = Encryption.decrypt(this.message);
        return name + " says: " + message;
    }
}
