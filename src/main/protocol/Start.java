package main.protocol;

import main.server.Connection;

import java.io.Serializable;

public class Start extends Sendable implements Serializable {
    private static final long serialVersionUID = 0L;

    private final String name;
    private boolean good;

    public Start(String name) {
        this.name = name;
        good = false;
    }

    @Override
    public void handle(Connection connection) {
        connection.acceptStart(this);
    }

    public boolean isGood() {
        return good;
    }

    public void setGood(boolean newBool) {
        good = newBool;
    }

    public String getName() {
        return name;
    }
}
